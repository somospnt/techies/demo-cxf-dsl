package com.somospnt.demo.cxf.dsl;

import javax.inject.Inject;
import org.apache.camel.builder.RouteBuilder;

public class SaludoRoute extends RouteBuilder {

    @Inject
    private PruebaService pruebaService;

    @Override
    public void configure() throws Exception {

        from("cxfrs:/api?resourceClasses=com.somospnt.demo.cxf.dsl.SaludoService&bindingStyle=SimpleConsumer")
                .toD("direct:${header.operationName}");

        from("direct:getSaludo").bean(pruebaService, "saludo").end();

        from("direct:getDespedida").bean(pruebaService, "despedida").end();
    }

}
