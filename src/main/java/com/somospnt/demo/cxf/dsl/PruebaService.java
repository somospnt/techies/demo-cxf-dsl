package com.somospnt.demo.cxf.dsl;

import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Named("pruebaService")
public class PruebaService {

    public Mensaje saludo() {
        return new Mensaje("Hola!!!!");
    }
    
    public Mensaje despedida() {
        return new Mensaje("Chau!!!!");
    }

}
