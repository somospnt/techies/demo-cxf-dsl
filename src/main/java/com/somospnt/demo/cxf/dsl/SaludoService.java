package com.somospnt.demo.cxf.dsl;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class SaludoService {

    @Path("/saludo")
    @GET()
    public Mensaje getSaludo() {
        return null;
    }

    @Path("/despedida")
    @GET()
    public Mensaje getDespedida() {
        return null;
    }

}
