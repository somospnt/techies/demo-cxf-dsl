## Compilación y despliegue

* Ejecutar `mvn clean install` para compilar.

* En la consola de Fuse instala la feature:
        
    `features:install pax-cdi pax-cdi-weld camel-cdi`  
    `install mvn:com.somospnt/demo-cxf-dsl/1.0-SNAPSHOT`  
    `start {id}`

* Comprueba que se ha instalado correctamente consultando el estado y las cabeceras del bundle.

	`la`  
	`headers [ID]`

		
## Ejemplo de uso

    `http://localhost:8181/cxf/api/saludo`  
    `http://localhost:8181/cxf/api/despedida`  
    



